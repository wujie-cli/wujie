#! /usr/bin/env node

// import { program } from "commander";
import { Command } from "commander";
const program = new Command();
// 引入package数据
import packageData from "../package.json" assert { type: "json" };
import fs from "fs-extra";

import { create } from "../lib/create.js";

// var version2 = "0.0.0";
// const getVersion = () => {
//   fs.readJson("./package.json", (err, packageObj) => {
//     if (err) console.error(err);
//     version2 = packageObj.version;
//     console.log(packageObj.version); // => 0.1.3
//   });
// };

program
  // 定义命令和参数
  .command("create <app-name>")
  .description("新建项目")
  // -f or --force 为强制创建，如果创建的目录存在则直接覆盖
  .option("-f, --force", "强制覆盖")
  .action((name, options) => {
    create(name, options);
    // 执行该命令
    // 打印执行结果
    // console.log("name:", name, "options:", options);
  });

program
  // 配置版本号信息
  .version(`v${packageData.version}`, "-V --version", "查询版本号")
  .name("wujie")
  // 首行提示
  .usage("<command> [option]");
// .action((name, options) => {
//   getVersion();
// });

// 解析用户执行命令传入参数
program.parse(process.argv);

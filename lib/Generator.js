// 处理项目创建逻辑

import { getRepoList, getTagList } from "./http.js";
import ora from "ora";
import select from "@inquirer/select";
import { input, confirm } from "@inquirer/prompts";
import path from "path";
import { download } from "./download.js"; // Promise
import fs from "fs-extra";
import { getTemplate } from "../templates/config.js";

// 添加加载动画
async function wrapLoading(fn, message, ...args) {
  // 使用 ora 初始化，传入提示信息 message
  // const spinner = ora(message).start();
  const spinner = ora("加载中...").start();
  // 开始加载动画
  spinner.start();

  try {
    // 执行传入方法 fn
    const result = await fn(...args);
    // 状态为修改为成功
    spinner.succeed();
    return result;
  } catch (error) {
    // 状态为修改为失败
    spinner.fail("Request failed, refetch ...");
  }
}

export class Generator {
  constructor(name, targetDir) {
    // 目录名称
    this.projectName = name;
    // 创建位置
    this.targetDir = targetDir;
  }

  // 获取用户选择的模板
  // 1）从远程拉取模板数据
  // 2）用户选择自己新下载的模板名称
  // 3）return 用户选择的名称
  async getRepo() {
    // 1）从远程拉取模板数据
    const repoList = await wrapLoading(getRepoList, "waiting fetch template");
    if (!repoList) return;
    // 过滤我们需要的模板名称
    // GITHUB 用这个
    // const repos = repoList.map((item) => ({ value: item.name }));
    // GITLAB 用这个
    const repos = repoList.map((item) => ({
      name: item.name,
      value: { id: item.id, name: item.path },
    }));
    // 2）用户选择自己新下载的模板名称
    const repo = await select({
      message: "请选择模板",
      choices: repos,
    });
    // 3）return 用户选择的名称
    return repo;
  }

  async getTag(id) {
    // 1）基于 repo 结果，远程拉取对应的 tag 列表
    const tagList = await wrapLoading(getTagList, "waiting fetch tag", id);
    if (!tagList) return;
    // 过滤我们需要的 tag 名称
    const tags = tagList.map((item) => ({ value: item.name }));
    // 2）用户选择自己需要下载的 tag
    const tag = await select({
      message: "请选择版本",
      choices: tags,
    });
    // 3）return 用户选择的 tag
    return tag;
  }

  // 下载远程模板
  // 1）拼接下载地址
  // 2）调用下载方法
  async downloadModule(name, tag) {
    // 1）拼接下载地址
    let requestUrl = `gitlab:wujie-cli/${name}${tag ? "#" + tag : ""}`;

    // 2）调用下载方法
    await wrapLoading(
      download, // 远程下载方法
      "waiting download template", // 加载提示信息
      requestUrl, // 参数1: 下载地址
      path.resolve(process.cwd(), this.targetDir), // 参数2: 创建位置
      { clone: true }
    );
  }

  // 初始化默认值
  async awakeConfig() {
    const awake = {
      MODULE_NAME_EN: await input({ message: "请输入模块名(英文)" }),
      // MODULE_NAME_ZH: await input({ message: "请输入模块名(中文)" }),
    };

    console.log(awake.MODULE_NAME_EN);
    // 模版文件目录 已弃用，从templates/config.js中引入
    // const destUrl = path.join(process.cwd(), "templates");
    // 生成文件目录
    const file = `${this.projectName}/config.ts`;

    // With Promises:
    fs.outputFile(file, getTemplate(awake.MODULE_NAME_EN))
      .then(() => fs.readFile(file, "utf8"))
      .then((data) => {
        // console.log(data);
      })
      .catch((err) => {
        // console.error(err);
      });
  }

  // 核心创建逻辑
  // 1）获取模板名称
  // 2）获取 tag 名称
  // 3）下载模板到模板目录
  async create() {
    // 1）获取模板名称
    const repo = await this.getRepo();
    // 2) 获取 tag 名称
    const tag = await this.getTag(repo.id);

    // console.log("用户选择了，repo=" + repo.name + "，tag=" + tag);

    // 3）下载模板到模板目录
    await this.downloadModule(repo.name, tag);

    // 替换config.ts模板
    await this.awakeConfig();

    // 4）模板使用提示
    console.log(`\r\n成功创建项目 ${this.projectName}`);
    console.log(`\r\n  cd ${this.projectName}`);
    console.log("  npm run dev\r\n");
  }
}

// 处理模板和版本信息的获取

// 通过 axios 处理请求
import axios from "axios";

axios.interceptors.response.use((res) => {
  return res.data;
});

/**
 * 获取模板列表
 * @returns Promise
 */
export async function getRepoList() {
  // *GITHUB 用这个
  // return axios.get("https://api.github.com/orgs/zhurong-cli/repos");
  // *GITLAB 用这个 （接口文档：https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects）
  return axios.get("https://gitlab.com/api/v4/groups/67258308/projects");
}

/**
 * 获取版本信息
 * @param {string} repo 模板名称
 * @returns Promise
 */
export async function getTagList(repo) {
  // *GITHUB 用这个
  // return axios.get(`https://api.github.com/repos/zhurong-cli/${repo}/tags`);
  // *GITLAB 用这个（接口文档：https://docs.gitlab.com/ee/api/tags.html#list-project-repository-tags）
  return axios.get(
    `https://gitlab.com/api/v4/projects/${repo}/repository/tags`
  );
}
